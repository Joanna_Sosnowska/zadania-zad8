#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Joanna667'

from django.db import connection
from django.contrib.auth.models import User
from microblog.models import Article, Tag
from django.test import TestCase
from django.utils import timezone
from django.test import Client
import re

login = "Joanna667"
haslo = "floreczka1"
client = Client()


#test, czy dany template istnieje - pusty blog, bez wpis�w
class TemplatesTestCase(TestCase):
    def test_admin(self):
        response = client.get("/admin/")
        #print response
        self.assertEqual(response.status_code, 200)

    def test_home(self):
        response = client.get("/blog/")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Najnowsze')



    def test_register_form(self):
        response = client.get("/blog/users/register")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'KONTO')


    def test_login_form(self):
        response = client.get("/blog/users/login")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Zaloguj')

    def test_add_article_form(self):
        response = client.get("/blog/create")
        print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'dla zalogowanych')


    def test_error(self):
        response = client.get("/blog/fiufiu")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'pusta')






class FuncionalityTestCase(TestCase):
    # baza danych - czy istnieje
    def test_database(self):
        cursor = connection.cursor()
        cursor.execute('SELECT * FROM microblog_article;')
        self.assertNotEqual(cursor.fetchall(), None)

    def test_messages(self):

        user = User.objects.create(username=login)
        user.set_password(haslo)
        user.save()
        tag = Tag(
            name='test'
        )
        tag.save()
        art = Article(
            tytul='tytul',
            tresc='tekst',
            pub_date=timezone.now(),
            pub_user=user

        )
        art.save()

        response = client.get("/blog/")

        self.assertContains(response, 'tytul')
        self.assertNotEqual(response.status_code, 404)
        self.assertNotEqual(response.content, None)
        
        response = client.get("/blog/tagged/1")

        self.assertContains(response, 'Posty z tagiem')
        self.assertNotEqual(response.status_code, 404)
        self.assertNotEqual(response.content, None)

        response = client.get("/blog/users/1")
        self.assertContains(response, login)
        self.assertNotEqual(response.status_code, 404)










